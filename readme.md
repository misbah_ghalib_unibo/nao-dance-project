# Alpha Team
Project of Fundamentals of AI in which NAO robot will dance following a choreography plan.

This repository contains the project of NAO robot dance which we have created for the subject of Fundamentals of AI, MS AI University of Bologna 21-22.

Our group consists of following members:
Misbah Ghalib
misbah.ghalib@studio.unibo.it

Rooshan Saleem Butt
rooshan.butt@studio.unibo.it

### Requirements:
1) Python 2+, Python 3+ installed
2) Choregraphe installed

### Instructions



On the Nao virtual machine, pull the repository to pull the code on local machine.

git clone https://misbah_ghalib_unibo@bitbucket.org/misbah_ghalib_unibo/nao-dance-project.git

Open Choregraphe for viewing the dance moves of NAO.
Copy the ip and the port of the simulated NAO and write below commands in the terminal.

cd nao-dance-project
cd project_files
chmod 777 ./startDance.sh
./startDance.sh --IP_ADDRESS --PORT

### Mechanism

We have used AIMA Library which implements the search algorithm for searching dance moves. A* search Algorithm has been used which takes the duration of each move as cost. The script calculate_duration.py calculates the duration of each move and stores in a JSON file. The NaoClass implements A* and stores the optimum moves in a text file which is later read by naoDance.py and moves are implemented on NAO.
