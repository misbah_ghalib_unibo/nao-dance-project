#!/bin/bash
/usr/bin/pip3 install --upgrade pip
/usr/bin/pip3 install -r requirements.txt
/usr/bin/pip install pygame

echo "Calculating the length of each position.."
python calculate_duration.py $1 $2

echo "Generating the best possible combination of moves using A* and Aima Library.."
python3 generateDanceSequence.py

echo "Checkout the moves of our Nao!"
python naoDance.py $1 $2 $3
